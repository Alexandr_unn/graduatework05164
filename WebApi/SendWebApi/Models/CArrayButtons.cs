﻿using System.Collections.Generic;

namespace SendWebApi.Models
{
    /// <summary>
    /// Объекта с массивом клавишь для контроллера SendButtonsController.
    /// </summary>
    public class CArrayButtons
    {
        //Массив клавишь.
        public List<string> buttons  { get; set; }

    }
}

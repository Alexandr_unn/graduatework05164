﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace SendWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    /// <summary>
    /// Контроллер для получения информации о системе.
    /// </summary>
    public class InfoSystemController : ControllerBase
    {
        /// <summary>
        /// Характеристики системы.
        /// </summary>
        public struct InfoSystem
        {
            /// <summary>
            /// Имя характеристики.
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Значение характеристики.
            /// </summary>
            public string Value { get; set; }
        }

        /// <summary>
        /// Получает информацию о системе.
        /// </summary>
        /// <returns> Массив с характеристика системе в формате Json</returns>
        [HttpGet]
        public InfoSystem[] GetInfoSystem()
        {
            // Вся информации о системе.   
            List<InfoSystem> list = new List<InfoSystem>();

            // Информации о локальных дисках системы.
            string[] drives = Environment.GetLogicalDrives();

            // Пара значений о системе: имя характеристики - значение характеристики.
            Dictionary<string, string> info_system = new Dictionary<string, string>
            {
                {"Operating system (version number)", Convert.ToString(Environment.OSVersion) },
                {"Computer name", Convert.ToString(Environment.MachineName) },
                {"Username ", Convert.ToString(Environment.UserName)},
                {"CPU model", Convert.ToString(Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER"))},
                {"CPU Architecture",Convert.ToString(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"))},
                {"Number of CPU cores",Convert.ToString(Environment.ProcessorCount) },
                {"Amount of RAM (MByte)",Convert.ToString(Environment.SystemPageSize) },
                {"Logical drives", String.Join(", ", drives) },
                {"Number of logical drives", Convert.ToString(drives.Length)},
                {"Uptime(milliseconds)", Convert.ToString(Environment.TickCount)}
            };
            
            foreach (var el in info_system)
            {
                list.Add(new InfoSystem { Name = el.Key, Value = el.Value});
            }

            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            Console.WriteLine("====Запрос GetInfoSystem====");
            Console.WriteLine("Ответ сервера:");
            // Ответ сервера для консоли.
            string answer = JsonSerializer.Serialize<List<InfoSystem>>(list,options);
            Console.WriteLine(answer);

            return list.ToArray();
        }

    }
}





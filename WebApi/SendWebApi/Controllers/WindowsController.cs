﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Text;

namespace SendWebApi.Controllers
{
	[ApiController]
    [Route("api/[controller]")]
	/// <summary>
	/// Контроллер для работы с окнами.
	/// </summary>
	public class WindowsController : ControllerBase
    {
		/// <summary>
		/// Переводит поток, который создал определяемое окно в приоритетный режим и активизирует окно.
		/// </summary>
		/// <param name="hWnd"> Дескриптор окна, которое переводится в приоритетный режим.</param>
		/// <returns>Если функция завершилась успешно, возвращается значение отличное от нуля.
		/// Если функция потерпела неудачу, возвращаемое значение - ноль.</returns>
		[DllImport("user32.dll")]
		static extern bool SetForegroundWindow(IntPtr hWnd);
		/// <summary>
		/// Устанавливает состояние показа определяемого окна.
		/// </summary>
		/// <param name="hwnd">Дескриптор окна, для показа. </param>
		/// <param name="nCmdShow"> Состояние показа окна. </param>
		/// <returns> Если функция завершилась успешно, возвращается значение отличное от нуля.
		/// Если функция потерпела неудачу, возвращаемое значение - ноль.</returns>
		[DllImport("user32")]
	     static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

		/// <summary>
		/// Oпределяет, не свернуто ли (минимизировано) определяемое окно.
		/// </summary>
		/// <param name="hWnd">Дескриптор окна для првоерки.</param>
		/// <returns>Если окно свёрнуто, функция возращает true, иначе false.</returns>
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool IsIconic(IntPtr hWnd);

		/// <summary>
		/// Характеристики открытого окна.
		/// </summary>
		public struct WindowInfo
		{
			/// <summary>
			/// Имя ведущего процесса.
			/// </summary>
			public string ProcessName { get; set; }

			/// <summary>
			/// Заголовок окна.
			/// </summary>
			public string Title { get; set; }
			/// <summary>
			/// Id окна.
			/// </summary>
			public int Id { get; set; }
		}

		/// <summary>
		/// Получение списка процессов имеющих заголовок главного окна.
		/// </summary>
		/// <returns> Массив значений в формате Json: имя процесса - заголовок главного окна процесса.</returns>
		[HttpGet]
		public WindowInfo[] GetProcess()
		{
			// Список всех процессов имеющих главное окно с заголовком.
			List<WindowInfo> list = new List<WindowInfo>();

			try
			{
				// Все запущенные процессы.
				Process[] ProcessesList = Process.GetProcesses();
				
				foreach (Process process in ProcessesList)
				{
					if (!string.IsNullOrEmpty(process.MainWindowTitle))
					{
						list.Add(new WindowInfo
						{
							ProcessName = process.ProcessName,
							Title = process.MainWindowTitle,
							Id = process.Id
						});
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}

			var options = new JsonSerializerOptions
			{
				WriteIndented = true
			};

			Console.WriteLine("====Запрос GetProcess====");
			Console.WriteLine("Ответ сервера:");
			// Ответ сервера для консоли.
			string answer = JsonSerializer.Serialize<List<WindowInfo>>(list, options);
			answer = Regex.Unescape(answer);
			Console.WriteLine(answer);

			return list.ToArray();
		}

		/// <summary>
		/// Перевод главного окна процесса в активное состояние.
		/// </summary>
		/// <param name="id"> Имя процесса окно, которого нужно перевести в активное состояние. </param>
		/// <returns> Если функция завершилась успешно, возвращается строка "Active window: " с именим процесса.
		/// Если функция потерпела неудачу, возвращаемое строку "Неверный параметр запроса" c именим процесса. </returns>
		[HttpGet("{id}")]
		public string ActiveWindow(int id)
		{
			try
			{
				// Все запущенные процессы.
				Process[] ProcessesList = Process.GetProcesses();

				// Процесс окно которого нужно перевести в активное состояние.
				Process active=null;

				foreach (Process el in ProcessesList)
				{
					if (el.Id == id)
					{
						active = el;
					}
				}

				if (active != null)
				{
					// Значение состояния показа окна.
					int SW_RESTORE = 9;

					Console.WriteLine("====Запрос GetProcess====");
					Console.WriteLine("Запрос от клиента: {0}", id);
					Console.WriteLine("Ответ сервера:");

					if(IsIconic(active.MainWindowHandle))
					{
						ShowWindow(active.MainWindowHandle, SW_RESTORE);
					}
					
					SetForegroundWindow(active.MainWindowHandle);
					Console.WriteLine("Active window: {0}", active.MainWindowTitle);
					return ("Active window: "+ active.MainWindowTitle);
				}
				else
				{
					Console.WriteLine("====Запрос GetProcess====");
					Console.WriteLine("Запрос от клиента: {0}", id);
					Console.WriteLine("Ответ сервера:");
					Console.WriteLine("Неверный параметр запроса {0}", id);
					return "Неверный параметр запроса \"" + id + "\"";
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return e.ToString();
			}
		}
	}
}

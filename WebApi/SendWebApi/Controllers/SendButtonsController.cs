﻿using Microsoft.AspNetCore.Mvc;
using System.Windows.Forms;
using SendWebApi.Models;
using System;
using System.Text.Json;
using System.Text.RegularExpressions;

using System.Collections.Generic;

namespace SendWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    /// <summary>
    /// Контроллер для работы с клавишами.
    /// </summary>
    public class SendButtonsController : ControllerBase
    {
        /// <summary>
        /// Нажатие клавишь в активном окне.
        /// </summary>
        /// <param name="obj"> Массив клавишь от клиента </param>
        /// <returns> Массив нажатых клавишь в формате Json </returns>
        [HttpPost]
        public string[] SendButtons(CArrayButtons obj )
        {
            // Ответ сервера.
            List <string> answer_server = new List<string>();
            try
            {
                foreach (string el in obj.buttons)
                {
                    SendKeys.SendWait(el);
                    answer_server.Add(el);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            // Опции для JsonSerializer.
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            Console.WriteLine("====Запрос SendButtons====");
            Console.WriteLine("Запрос от клиента:");
            
            //Запрос клиента для консоли.
            string request = JsonSerializer.Serialize<List<string>>(obj.buttons, options);
            request = Regex.Unescape(request);
            Console.WriteLine(request);
            Console.WriteLine("Ответ сервера:");

            //Ответ сервера для консоли.
            string answer = JsonSerializer.Serialize<List<string>>(answer_server, options);
            answer = Regex.Unescape(answer);
            Console.WriteLine(answer);

            return answer_server.ToArray();
        }
    }
}

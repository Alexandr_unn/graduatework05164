﻿using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;


namespace SendWebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var pathToExe = Process.GetCurrentProcess().MainModule.FileName;

            var pathToContentRoot = Path.GetDirectoryName(pathToExe);

            var host = WebHost.CreateDefaultBuilder(args)
                    .UseContentRoot(pathToContentRoot)
                    .UseStartup<Startup>()
                    .Build();
            host.Run();
        }
    }
}

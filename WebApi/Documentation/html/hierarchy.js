var hierarchy =
[
    [ "ControllerBase", null, [
      [ "SendWebApi.Controllers.InfoSystemController", "class_send_web_api_1_1_controllers_1_1_info_system_controller.html", null ],
      [ "SendWebApi.Controllers.SendButtonsController", "class_send_web_api_1_1_controllers_1_1_send_buttons_controller.html", null ],
      [ "SendWebApi.Controllers.TestController", "class_send_web_api_1_1_controllers_1_1_test_controller.html", null ],
      [ "SendWebApi.Controllers.WindowsController", "class_send_web_api_1_1_controllers_1_1_windows_controller.html", null ]
    ] ],
    [ "SendWebApi.Controllers.InfoSystemController.InfoSystem", "struct_send_web_api_1_1_controllers_1_1_info_system_controller_1_1_info_system.html", null ],
    [ "SendWebApi.Controllers.WindowsController.WindowInfo", "struct_send_web_api_1_1_controllers_1_1_windows_controller_1_1_window_info.html", null ]
];
var dir_954a9df7ca4ed9fd64845862dc8f6e0c =
[
    [ "InfoSystemController.cs", "_info_system_controller_8cs.html", [
      [ "InfoSystemController", "class_send_web_api_1_1_controllers_1_1_info_system_controller.html", "class_send_web_api_1_1_controllers_1_1_info_system_controller" ],
      [ "InfoSystem", "struct_send_web_api_1_1_controllers_1_1_info_system_controller_1_1_info_system.html", "struct_send_web_api_1_1_controllers_1_1_info_system_controller_1_1_info_system" ]
    ] ],
    [ "SendButtonsController.cs", "_send_buttons_controller_8cs.html", [
      [ "SendButtonsController", "class_send_web_api_1_1_controllers_1_1_send_buttons_controller.html", "class_send_web_api_1_1_controllers_1_1_send_buttons_controller" ]
    ] ],
    [ "TestController.cs", "_test_controller_8cs.html", [
      [ "TestController", "class_send_web_api_1_1_controllers_1_1_test_controller.html", "class_send_web_api_1_1_controllers_1_1_test_controller" ]
    ] ],
    [ "WindowsController.cs", "_windows_controller_8cs.html", [
      [ "WindowsController", "class_send_web_api_1_1_controllers_1_1_windows_controller.html", "class_send_web_api_1_1_controllers_1_1_windows_controller" ],
      [ "WindowInfo", "struct_send_web_api_1_1_controllers_1_1_windows_controller_1_1_window_info.html", "struct_send_web_api_1_1_controllers_1_1_windows_controller_1_1_window_info" ]
    ] ]
];
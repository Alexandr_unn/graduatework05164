﻿namespace MyAppConfig.FileSystemStream
{
	public interface IAppConfigPathExtractor
	{
		string Path
		{
			get;
		}
	}
}
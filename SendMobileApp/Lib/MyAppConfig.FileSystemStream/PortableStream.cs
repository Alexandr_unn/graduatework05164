﻿using System;
using System.IO;

namespace MyAppConfig.FileSystemStream
{
    public static class PortableStream
	{
		private static readonly Lazy<Stream> _appConfigStream = new Lazy<Stream>(CreateAppConfigStream,
			System.Threading.LazyThreadSafetyMode.PublicationOnly);
		public static Stream Current
		{
			get
			{
				var result = _appConfigStream.Value;
				return result;
			}
		}
		private static Stream CreateAppConfigStream()
		{
			var pathExtractor = CreateAppConfigPathExtractor();
			return GetStream(pathExtractor.Path);
		}
		private static IAppConfigPathExtractor CreateAppConfigPathExtractor()
		{
#if ANDROID
			return new AndroidAppConfigPathExtractor();
#elif __IOS__
			return new IOSAppConfigPathExtractor();
#else
			return null;
#endif
		}
	    private static Stream GetStream(string configPath)
	    {
	        if (!File.Exists(configPath))
	        {
	            var dllConfigPath = configPath.Replace("iOS.exe", "dll");       
	            var dllConfigPathExists = File.Exists(dllConfigPath);
	            if (dllConfigPathExists)
	                return File.OpenRead(dllConfigPath);
	            throw new FileNotFoundException($"path: {configPath} and {dllConfigPath}");
	        }
	        return File.OpenRead(configPath);
	    }
	}
}
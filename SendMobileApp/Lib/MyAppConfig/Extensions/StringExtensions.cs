﻿using System.Text.RegularExpressions;

namespace MyAppConfig.Extensions
{
    public static class StringExtensions
    {
        private static string Pascalize(this string input)
        {
            return Regex.Replace(input, "(?:^|_)(.)", (MatchEvaluator)(match => match.Groups[1].Value.ToUpper()));
        }
        public static string Camelize(this string input)
        {
            string str = input.Pascalize();
            return str.Substring(0, 1).ToLower() + str.Substring(1);
        }
    }
}

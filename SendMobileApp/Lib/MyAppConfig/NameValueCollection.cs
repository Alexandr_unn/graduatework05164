﻿using MyAppConfig.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MyAppConfig
{
	public interface INameValueElement<T>
	{
		string Key { get; set; }
		T Value { get; set; }
	}

	public class NameValueCollection<TMapped> where TMapped : INameValueElement<string>
	{
		public NameValueCollection()
		{ }
		public NameValueCollection(IList<TMapped> datas)
		{
			this.datas = datas;
		}
		public string this[string name]
		{
			get
			{
				IEnumerable<string> values = datas.Where(x => x.Key == name).Select(x => x.Value);
				return values == null ? null : string.Join(",", values);
			}
		}
		public string Get(string name)
		{
			return this[name];
		}
		public void Set(string name,string value)
		{
			try
			{
				var settings = MyAppConfig.ConfigurationManager.AppSettings.datas;
				Setting test = new Setting();
				int count = 0;
				for (int i = 0; i < settings.Count; i++)
				{
					if (settings[i].Key == name)
					{
						test = settings[i];
						break;
					}
					count++;
				}

				if (!settings.Contains(test))
				{
					Setting item = new Setting();
					item.Key = name;
					item.Value = value;
					settings.Add(item);
				}
				else
				{
					settings[count].Value = value;    
				}
			}
			catch
			{
				throw new Exception("Error. Unable to change values.");
			}
		}
		public string[] GetValues(string name)
		{
			return datas.Where(x => x.Key == name).Select(x => x.Value)?.ToArray();
		}
		public string Save()
		{
			string  result = ConfigurationManager.Save(null);
			return result;
		}
		private IList<TMapped> datas;
	}
}

﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SendMobileApp
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _ipconfigText = "";

        public string Ip
        {
            get { return _ipconfigText; }
            set
            {
                if (_ipconfigText == value) return;
                    _ipconfigText = value;
                    OnPropertyChanged(nameof(Ip));
            }
        }
        private string _portconfigText = "";
        public string Port
        {
            get { return _portconfigText; }
            set
            {
                if (_portconfigText == value) return;
                    _portconfigText = value;
                    OnPropertyChanged(nameof(Port));
            }
        }

        public string GetPortSetting()
        {
            Port = MyAppConfig.ConfigurationManager.AppSettings.Get("Port");
            return Port;
        }

        public string GetIpSetting()
        {
            Ip = MyAppConfig.ConfigurationManager.AppSettings.Get("Ip");
            return Ip;
        }
        public MainViewModel()
        {
            GetIpSetting();
            GetPortSetting();
        }
    }
}

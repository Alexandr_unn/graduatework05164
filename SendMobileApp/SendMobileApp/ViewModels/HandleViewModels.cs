﻿using SendMobileApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SendMobileApp.ViewModels
{
    class HandleViewModels : BaseViewModel
    {
        static readonly HttpClient HandelClient1 = new HttpClient();
        public ObservableCollection<Handleinfo> Items { get; set; }
        public Command HandleItemsCommand { get; set; }
        public List<Handleinfo> procinfo;

        public HandleViewModels()
        {
            procinfo = new List<Handleinfo>();
            Items = new ObservableCollection<Handleinfo>();
            HandleItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }
        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                Items.Clear();
                var items = await StartFuncAsync();
                foreach (var item in procinfo)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        async Task<string> StartFuncAsync()
        {
            string urlstart = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/windows";
            string responseBody = "";
            procinfo.Clear();
            try
            {
                HttpResponseMessage response1 = await HandelClient1.GetAsync(urlstart);
                response1.EnsureSuccessStatusCode();
                responseBody = await response1.Content.ReadAsStringAsync();
                procinfo = Handleinfo.FromJson(responseBody);
            }
            catch (HttpRequestException a)
            {
                responseBody = a.Message;
            }
            return responseBody;
        }
    }
}

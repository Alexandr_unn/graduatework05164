﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using Xamarin.Forms;

namespace SendMobileApp
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        readonly static HttpClient client = new HttpClient();
        public Dictionary<string, string> openWith = new Dictionary<string, string>();
        public MainPage()
        {
            try
            {
                client.Timeout = TimeSpan.FromSeconds(2);
                InitializeComponent();
            }
            catch (InvalidOperationException soe)
            {
                if (!soe.Message.Contains("MUST"))
                    throw;
            }
            BindingContext = new MainViewModel();
        }
        private void _ipedit_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.OldTextValue != e.NewTextValue)
            {
                MyAppConfig.ConfigurationManager.AppSettings.Set("Ip", e.NewTextValue);
            }
            Save();
        }
        private void _portedit_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.OldTextValue != e.NewTextValue)
            {
                MyAppConfig.ConfigurationManager.AppSettings.Set("Port", e.NewTextValue);
            }
            Save();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void TestConnect(object sender, EventArgs e)
        {
            string url1 = "http://" + _ipedit.Text + ":" + _portedit.Text + "/api/infosystem";
            try
            {
                HttpResponseMessage message = await client.GetAsync(url1);
                message.EnsureSuccessStatusCode();
                string responseBody = await message.Content.ReadAsStringAsync();
                if(message.IsSuccessStatusCode)
                   await Navigation.PushAsync(new SelectPage());
            }
            catch(HttpRequestException a)
            {
                await DisplayAlert("Ошибка подключения", a.Message, "Ok");
            }
            catch
            {
                await DisplayAlert("Ошибка подключения", "Сервер не доступен", "Ok");
            }
        }
        public void Save()
        {
            string qwe = MyAppConfig.ConfigurationManager.Save(null);
        }
    }
}

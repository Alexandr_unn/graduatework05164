﻿using SendMobileApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendMobileApp.Services
{
    class ComDataStore
    {
        public List<SendComande> comande;
        public ComDataStore()
        {
            comande = new List<SendComande>();

            var mockComande = new List<SendComande>
            {
                new SendComande { Name = "BACKSPACE", Action = "BS", },
                new SendComande { Name = "BREAK", Action = "BREAK"},
                new SendComande { Name =  "CAPS LOCK", Action = "CAPSLOCK" },
                new SendComande { Name = "DEL or DELETE", Action = "DEL" },
                new SendComande { Name = "DOWN ARROW", Action = "DOWN" },
                new SendComande { Name = "END", Action = "END"},
                new SendComande { Name = "ENTER", Action = "ENTER" },
                new SendComande { Name = "ESC", Action = "ESC" },
                new SendComande { Name = "HELP", Action = "HELP" },
                new SendComande { Name = "HOME", Action = "HOME"},
                new SendComande { Name = "INS or INSERT", Action = "INS" },
                new SendComande { Name = "LEFT ARROW", Action = "LEFT" },
                new SendComande { Name = "NUM LOCK", Action = "NUMLOCK" },
                new SendComande { Name = "PAGE DOWN", Action = "PGDN"},
                new SendComande { Name = "PAGE UP", Action = "PGUP" },
                new SendComande { Name = "PRINT SCREEN", Action = "PRTSC" },
                new SendComande { Name = "RIGHT ARROW", Action = "RIGHT" },
                new SendComande { Name = "SCROLL LOCK", Action = "SCROLLLOCK"},
                new SendComande { Name = "TAB", Action = "TAB" },
                new SendComande { Name = "UP ARROW", Action = "UP" },
                new SendComande { Name = "F1", Action = "F1" },
                new SendComande { Name = "F2", Action = "F2" },
                new SendComande { Name = "F3", Action = "F3" },
                new SendComande { Name = "F4", Action = "F4" },
                new SendComande { Name = "F5", Action = "F5" },
                new SendComande { Name = "F6", Action = "F6" },
                new SendComande { Name = "F7", Action = "F7" },
                new SendComande { Name = "F8", Action = "F8" },
                new SendComande { Name = "F9", Action = "F9" },
                new SendComande { Name = "F10", Action = "F10" },
                new SendComande { Name = "F11", Action = "F11" },
                new SendComande { Name = "F12", Action = "F12" },
                new SendComande { Name = "SHIFT", Action = "shift" },
                new SendComande { Name = "CTRL", Action = "ctrl" },
                new SendComande { Name = "ALT", Action = "alt" },
                new SendComande { Name = "Закрыть приложение ALT+F4", Action = "exit" },
            };
            foreach (var com in mockComande)
            {
                comande.Add(com);
            }
        }
    }
}

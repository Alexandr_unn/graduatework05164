﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using SendMobileApp.Models;


namespace SendMobileApp.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>();
            var mockItems = new List<Item>
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "Управление презентацией" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Управление активными окнами" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Информация о системе" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Сетевые адаптеры" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Отправка содержимого" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Отправка команд"}
            };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}
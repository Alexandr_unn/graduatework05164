﻿namespace SendMobileApp.Models.TraficInfo
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class TraficInfo
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("speed")]
        public string Speed { get; set; }

        [JsonProperty("bytesReceived")]
        public string BytesReceived { get; set; }

        [JsonProperty("bytesSent")]
        public string BytesSent { get; set; }

        [JsonProperty("incomingPacketsDiscarded")]
        public string IncomingPacketsDiscarded { get; set; }

        [JsonProperty("incomingPacketsWithErrors")]
        public string IncomingPacketsWithErrors { get; set; }

        [JsonProperty("incomingUnknownProtocolPackets")]
        public string IncomingUnknownProtocolPackets { get; set; }

        [JsonProperty("nonUnicastPacketsReceived")]
        public string NonUnicastPacketsReceived { get; set; }

        [JsonProperty("nonUnicastPacketsSent")]
        public string NonUnicastPacketsSent { get; set; }

        [JsonProperty("outgoingPacketsDiscarded")]
        public string OutgoingPacketsDiscarded { get; set; }

        [JsonProperty("outgoingPacketsWithErrors")]
        public string OutgoingPacketsWithErrors { get; set; }

        [JsonProperty("outputQueueLength")]
        public string OutputQueueLength { get; set; }

        [JsonProperty("unicastPacketsReceived")]
        public string UnicastPacketsReceived { get; set; }

        [JsonProperty("unicastPacketsSent")]
        public string UnicastPacketsSent { get; set; }

    }

    public partial class TraficInfo 
    {
        public static List<TraficInfo> FromJson(string json) => JsonConvert.DeserializeObject<List<TraficInfo>>(json, SendMobileApp.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<TraficInfo> self) => JsonConvert.SerializeObject(self, SendMobileApp.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

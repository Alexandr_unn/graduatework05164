﻿namespace SendMobileApp.View.Content
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ComandeJS
    {
        [JsonProperty("buttons")]
        public List<string> Buttons { get;  set; }
    }

    public partial class ComandeJS
    {
        public static ComandeJS FromJson(string json) => JsonConvert.DeserializeObject<ComandeJS>(json, SendMobileApp.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ComandeJS self) => JsonConvert.SerializeObject(self, SendMobileApp.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}


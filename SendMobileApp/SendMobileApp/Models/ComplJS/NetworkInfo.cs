﻿namespace SendMobileApp.Models.ComplJS
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class NetworkInfo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("interfaceType")]
        public string InterfaceType { get; set; }

        [JsonProperty("physicalAddress")]
        public string PhysicalAddress { get; set; }

        [JsonProperty("operationalStatus")]
        public string OperationalStatus { get; set; }

        [JsonProperty("ipVersion")]
        public string IpVersion { get; set; }

        [JsonProperty("dnsSuffix")]
        public string DnsSuffix { get; set; }

        [JsonProperty("mtu")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Mtu { get; set; }

        [JsonProperty("dnsEnabled")]
        public string DnsEnabled { get; set; }

        [JsonProperty("dynamicallyConfiguredDNS")]
        public string DynamicallyConfiguredDns { get; set; }

        [JsonProperty("receiveOnly")]
        public string ReceiveOnly { get; set; }

        [JsonProperty("multicast")]
        public string Multicast { get; set; }
    }

    public partial class NetworkInfo
    {
        public static List<NetworkInfo> FromJson(string json) => JsonConvert.DeserializeObject<List<NetworkInfo>>(json, InfoSystem.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<NetworkInfo> self) => JsonConvert.SerializeObject(self, InfoSystem.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}

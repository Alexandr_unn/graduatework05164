﻿namespace SendMobileApp
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Handleinfo
    {
        [JsonProperty("processName")]
        public string ProcessName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class Handleinfo
    {
        public static List<Handleinfo> FromJson(string json) => JsonConvert.DeserializeObject<List<Handleinfo>>(json, SendMobileApp.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<Handleinfo> self) => JsonConvert.SerializeObject(self, SendMobileApp.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

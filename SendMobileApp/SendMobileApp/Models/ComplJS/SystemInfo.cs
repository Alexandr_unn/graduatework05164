﻿namespace InfoSystem
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class SystemInfo 
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class SystemInfo
    {
        public static List<SystemInfo> FromJson(string json) => JsonConvert.DeserializeObject<List<SystemInfo>>(json, InfoSystem.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<SystemInfo> self) => JsonConvert.SerializeObject(self, InfoSystem.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

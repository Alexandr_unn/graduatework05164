﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SendMobileApp.Services
{
    public class ItemSysInfo
    {
        public string Title { get; set; }
        public string Data { get; set; }
        public string ImagePath { get; set; }
    }
}

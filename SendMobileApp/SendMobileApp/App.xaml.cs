﻿using Xamarin.Forms;
using MyAppConfig;

namespace SendMobileApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            ConfigurationManager.Initialise(MyAppConfig.FileSystemStream.PortableStream.Current);

            MainPage = new NavigationPage(new MainPage());
        }
    }
}

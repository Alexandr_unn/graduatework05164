﻿using SendMobileApp.Models.TraficInfo;
using SendMobileApp.Services;
using SendMobileApp.View.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SendMobileApp.View.Modal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TraficInfoPage : ContentPage
    {
        static List<TraficInfo> trafinfo;
        static readonly HttpClient qqqq = new HttpClient();
        static PropertyInfo[] properties;
        int pagedetail;
        public TraficInfoPage()
        {
            pagedetail = NetworkInterfacePage.controlnumber;
            Type tp = typeof(TraficInfo);
            properties = tp.GetProperties();
            trafinfo = new List<TraficInfo>();
            InitializeComponent();
            StartFuncAsync();
            Basepage.Title = "Трафик адаптера";
        }
        async void StartFuncAsync()
        {
            string url1 = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/NetworkInterface";
            try
            {
                url1 += "/"+ pagedetail + "";
                HttpResponseMessage response12 = await qqqq.GetAsync(url1);
                response12.EnsureSuccessStatusCode();
                string responseBody = await response12.Content.ReadAsStringAsync();
                trafinfo = TraficInfo.FromJson(responseBody);
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
            Trafic.ItemsSource = GetTraficInfo();
        }
        static public List<ItemSysInfo> GetTraficInfo()
        {
            List<ItemSysInfo> retlis = new List<ItemSysInfo>();

            retlis.Add(new ItemSysInfo { Title = properties[0].Name, Data = trafinfo[0].Description });
            retlis.Add(new ItemSysInfo { Title = properties[1].Name, Data = trafinfo[0].Speed });
            retlis.Add(new ItemSysInfo { Title = properties[2].Name, Data = trafinfo[0].BytesReceived });
            retlis.Add(new ItemSysInfo { Title = properties[3].Name, Data = trafinfo[0].BytesSent });
            retlis.Add(new ItemSysInfo { Title = properties[4].Name, Data = trafinfo[0].IncomingPacketsDiscarded });
            retlis.Add(new ItemSysInfo { Title = properties[5].Name, Data = trafinfo[0].IncomingPacketsWithErrors });
            retlis.Add(new ItemSysInfo { Title = properties[6].Name, Data = trafinfo[0].IncomingUnknownProtocolPackets });
            retlis.Add(new ItemSysInfo { Title = properties[7].Name, Data = trafinfo[0].NonUnicastPacketsReceived });
            retlis.Add(new ItemSysInfo { Title = properties[8].Name, Data = trafinfo[0].NonUnicastPacketsSent });
            retlis.Add(new ItemSysInfo { Title = properties[9].Name, Data = trafinfo[0].OutgoingPacketsDiscarded });
            retlis.Add(new ItemSysInfo { Title = properties[10].Name, Data = trafinfo[0].OutgoingPacketsWithErrors });
            retlis.Add(new ItemSysInfo { Title = properties[11].Name, Data = trafinfo[0].OutputQueueLength });
            retlis.Add(new ItemSysInfo { Title = properties[12].Name, Data = trafinfo[0].UnicastPacketsReceived });
            retlis.Add(new ItemSysInfo { Title = properties[13].Name, Data = trafinfo[0].UnicastPacketsSent });

            return retlis;
        }
    }
}
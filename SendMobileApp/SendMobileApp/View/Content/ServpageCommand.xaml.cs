﻿using SendMobileApp.Services;
using SendMobileApp.View.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SendMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        static readonly HttpClient ClientComande = new HttpClient();
        string url = "";
        public Dictionary<string, string> openWith = new Dictionary<string, string>();
        ComandeJS ActionComande;
        public Page1()
        {
            ActionComande = new ComandeJS();
            InitializeComponent();
            StartFuncAsync();
        }
        /// <summary>
        /// Заполняю поля пикера доступными командами.
        /// </summary>
        /// 
        void ServerCommands(object sender, EventArgs e)
        {
            if (PickerComm.Items.Count == 0)
            {
                string ttt = "";
                for (int i = 1; i < openWith.Count; i++)
                {
                    ttt = openWith.Keys.ElementAt(i);
                    PickerComm.Items.Add(ttt);
                }
            }
        }

        /// <summary>
        /// Получаю команды с сервера web api.
        /// </summary>
        void StartFuncAsync()
        {
            openWith.Clear();

            var Comandes = new ComDataStore();

            foreach (var com in Comandes.comande)
            {
                openWith.Add(com.Name, com.Action);
            }
            string ttt = "";
            for (int i = 1; i < openWith.Count; i++)
            {
                ttt = openWith.Keys.ElementAt(i);
                PickerComm.Items.Add(ttt);
            }
        }
        /// <summary>
        /// Отправка команд на сервер 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void ButSayServer_Async(object sender, EventArgs e)
        {
            ActionComande.Buttons = new List<string>();
            url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/SendButtons/";
            string messagetext = "";
            messagetext = openWith[PickerComm.SelectedItem.ToString()];
            try
            {
                if (messagetext.Length != 0)
                {
                    if(messagetext == "exit") ActionComande.Buttons.Add("%{F4}");
                    else ActionComande.Buttons.Add("{" + messagetext + "}");  
                    HttpResponseMessage response = await ClientComande.PostAsJsonAsync(url, ActionComande);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
               }
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
            ActionComande.Buttons.Clear();
        }
    }
}
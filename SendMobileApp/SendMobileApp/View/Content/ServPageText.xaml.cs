﻿using Newtonsoft.Json;
using SendMobileApp.View.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Formatting;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SendMobileApp.Services;
using System.Configuration;
using SendMobileApp.ViewModels;
using MyAppConfig;

namespace SendMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServPageText : ContentPage
    {
        static readonly HttpClient ComandeClient = new HttpClient();
        string url = "";
        public Dictionary<string, string> openWith = new Dictionary<string, string>(); // Словарь для хранения пары значений текст/команда для удобства выбора 
        public ServPageText()
        {
            InitializeComponent();
            StartFuncAsync();
        }

        /// <summary>
        /// Добавление команды в поле ввода.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ItemPickerSelected(object sender, EventArgs e)
        {
            if ((sender as Picker).SelectedIndex != -1) 
            {
                string strcomand = "";
                strcomand = openWith[PickerComm.SelectedItem.ToString()];
                EditorText.Text += "{" + strcomand + "}";
            }
        }
        /// <summary>
        /// Отправляю текст Post запросами на контролы web api.
        /// Создаю объект класса ComandeJS.
        /// Заполняю командами в порядке следования в поле ввода.
        /// Отправляю объект web api.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void Butsend_ClickedAsync(object sender, EventArgs e)
        {
            url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/sendbuttons/";
            char[] messagetext;
            messagetext = EditorText.Text.ToCharArray();
            ComandeJS ActionComande = new ComandeJS();
            string buff = "";
            string buff1 = "";
            bool check1 = false;
            bool check2 = false;
            try
            {
                if (messagetext.Length != 0)
                {
                    ActionComande.Buttons = new List<string>();
                    int step = 0;
                    do
                    {
                        if (messagetext[step] != '{')
                        {
                            ActionComande.Buttons.Add(messagetext[step].ToString());
                            step++;
                        }
                        else if (messagetext[step] == '{')
                        {
                            do
                            {
                                buff += messagetext[step];
                                step++;
                            } while (messagetext[step] != '}');
                            buff += '}';
                            step++;
                            check1 = true;
                        }
                        if (step !=messagetext.Length && messagetext[step] == '+')
                        {
                            step++; 
                            do
                            {
                                buff1 += messagetext[step];
                                step++;
                            } while (messagetext[step] != '}');
                            buff1 += '}';
                            step++;
                            check2 = true;
                        }
                        if (check1 == true && check2 == false)
                        {
                            if(buff == "exit") ActionComande.Buttons.Add("%{F4}");
                            else ActionComande.Buttons.Add(buff);
                        }
                        else if (check1 == true && check2 == true)
                        {
                            buff += "+" + buff1;
                            ActionComande.Buttons.Add(buff);
                        }
                        buff = "";
                        buff1 =  "";
                    } while (step != messagetext.Length);
                    HttpResponseMessage response = await ComandeClient.PostAsJsonAsync(url, ActionComande);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();  
                }
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
            ActionComande.Buttons.Clear();
        }
        /// <summary>
        /// Получаю команды с сервера web api.
        /// </summary>
        void StartFuncAsync()
        {
            openWith.Clear();

            var Comandes = new ComDataStore();

            foreach (var com in Comandes.comande)
            {
                openWith.Add(com.Name, com.Action);
            }
                        
            string ttt = "";
            for (int i = 1; i < openWith.Count; i++)
            {
                ttt = openWith.Keys.ElementAt(i);
                PickerComm.Items.Add(ttt);
            }
        }
        private void PickerComm_Focused(object sender, FocusEventArgs e)
        {
            PickerComm.SelectedIndex = -1;
        }
    }
}
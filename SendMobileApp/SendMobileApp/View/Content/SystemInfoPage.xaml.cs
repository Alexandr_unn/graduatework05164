﻿using InfoSystem;
using SendMobileApp.Services;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using System.Collections.Generic;

namespace SendMobileApp.View.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SystemInfoPage : ContentPage
    {
        static readonly HttpClient HandelClient = new HttpClient();

        List<ItemSysInfo> sysinfolist;
        List<SystemInfo> sysinfo;

        public SystemInfoPage()
        {
            sysinfo = new List<SystemInfo>();
            sysinfolist = new List<ItemSysInfo>();

            InitializeComponent();

            sysinfolist = SelectPage.GetSysInfo();
            SystemInfoList.ItemsSource = sysinfolist;
        }
    }
}

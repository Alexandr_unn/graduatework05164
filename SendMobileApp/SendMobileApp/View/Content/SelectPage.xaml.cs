﻿using InfoSystem;
using SendMobileApp.Models;
using SendMobileApp.Services;
using SendMobileApp.View.Content;
using SendMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SendMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectPage : ContentPage
    {
        ItemsViewModel viewModel;
        static readonly HttpClient HandelClient = new HttpClient();
        static List<SystemInfo> sysinfo;
        public SelectPage()
        {
            sysinfo = new List<SystemInfo>();
            InitializeComponent();
            StartFuncAsync();
            BindingContext = viewModel = new ItemsViewModel();
        }
        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var items = args.SelectedItem as Item;
            if (items == null)
                return;
            if (items.Text == "Управление презентацией")
            {
                await Navigation.PushAsync(new PowerPointPage());
            }
            if (items.Text == "Отправка содержимого")
            {
                await Navigation.PushAsync(new ServPageText());
            }
            if (items.Text == "Отправка команд")
            {
                await Navigation.PushAsync(new Page1());
            }
            if (items.Text == "Управление активными окнами")
            {
                await Navigation.PushAsync(new HandlePage());
            }
            if (items.Text == "Информация о системе")
            {
                await Navigation.PushAsync(new SystemInfoPage());
            }
            if (items.Text == "Сетевые адаптеры")
            {
                await Navigation.PushAsync(new NetworkInterfacePage());
            }
            // Manually deselect item.
            ItemsListView.SelectedItem = null;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
        /// <summary>
        /// Заполняем информацию о системе.
        /// </summary>
        /// <returns></returns>
        static public List<ItemSysInfo> GetSysInfo()
        {
            List<ItemSysInfo> retlis = new List<ItemSysInfo>();

            retlis.Add(new ItemSysInfo { Title = sysinfo[0].Name, Data = sysinfo[0].Value, ImagePath = "os.png" });
            retlis.Add(new ItemSysInfo { Title = sysinfo[1].Name, Data = sysinfo[1].Value, ImagePath = "pc.png" });
            retlis.Add(new ItemSysInfo { Title = sysinfo[2].Name, Data = sysinfo[2].Value, ImagePath = "user.png" });
            retlis.Add(new ItemSysInfo { Title = sysinfo[3].Name, Data = sysinfo[3].Value, ImagePath = "cpu.png" });
            if (sysinfo[4].Value == "x64")
            {
                retlis.Add(new ItemSysInfo { Title = sysinfo[4].Name, Data = sysinfo[4].Value, ImagePath = "icons64bit.png" });
            }
            else
            {
                retlis.Add(new ItemSysInfo { Title = sysinfo[4].Name, Data = sysinfo[4].Value, ImagePath = "icons32bit.png" });
            }
            retlis.Add(new ItemSysInfo { Title = sysinfo[5].Name, Data = sysinfo[5].Value, ImagePath = "processorCore.png" });
            retlis.Add(new ItemSysInfo { Title = sysinfo[6].Name, Data = sysinfo[6].Value, ImagePath = "ram.png" });
            retlis.Add(new ItemSysInfo { Title = sysinfo[7].Name, Data = "Количество: " + sysinfo[8].Value + "      " + sysinfo[7].Value, ImagePath = "hdd.png" });
            retlis.Add(new ItemSysInfo { Title = sysinfo[9].Name, Data = sysinfo[9].Value, ImagePath = "time.png" });

            return retlis;
        }

        /// <summary>
        /// Получаю команды с сервера web api.
        /// </summary>
        async void StartFuncAsync()
        {
            string url1 = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/infosystem";           
            try
            {
                HttpResponseMessage response1 = await HandelClient.GetAsync(url1);
                response1.EnsureSuccessStatusCode();
                string responseBody = await response1.Content.ReadAsStringAsync();
                sysinfo = SystemInfo.FromJson(responseBody);
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
        }
    }
}
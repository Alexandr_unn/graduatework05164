﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SendMobileApp.View.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PowerPointPage : ContentPage
    {
        static readonly HttpClient PowerPointClient = new HttpClient();
        string url = "";
        ComandeJS comande;
        public PowerPointPage()
        {
            comande = new ComandeJS();
            comande.Buttons = new List<string>();
            InitializeComponent();
        }
        /// <summary>
        /// При нажатии на кнопку, происходит передача команды F5 (начать просмотр слайдов).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async private void ButtonClickedBindig(object sender, EventArgs e)
        {
            if ((sender as Button).Text == "Начать показ слайдов \n(F5)")
            {
                url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/sendbuttons/";
                string textcomande = "{F5}";
                try
                {
                    comande.Buttons.Add(textcomande);
                    HttpResponseMessage response = await PowerPointClient.PostAsJsonAsync(url, comande);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                }
                catch (HttpRequestException a)
                {
                    await DisplayAlert("Exception ", a.Message, "Ok");
                }
                comande.Buttons.Clear();
                (sender as Button).Text = "Завершить показ слайдов \n(ESC)";
            }
            else if((sender as Button).Text == "Завершить показ слайдов \n(ESC)")
            {
                url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/sendbuttons/";
                string textcomande = "{ESC}";
                try
                {
                    comande.Buttons.Add(textcomande);
                    HttpResponseMessage response = await PowerPointClient.PostAsJsonAsync(url, comande);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                }
                catch (HttpRequestException a)
                {
                    await DisplayAlert("Exception ", a.Message, "Ok");
                }
                comande.Buttons.Clear();
                (sender as Button).Text = "Начать показ слайдов \n(F5)";
            }
        }
        /// <summary>
        /// При нажатии на кнопку, происходит передача команды PGUP (переключить следующий слайд).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async private void ButtonPrev_Clicked(object sender, EventArgs e)
        {
            url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/sendbuttons/";
            string textcomande = "{PGUP}";
            try
            {
                comande.Buttons.Add(textcomande);
                HttpResponseMessage response = await PowerPointClient.PostAsJsonAsync(url, comande);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
            comande.Buttons.Clear();
        }
        /// <summary>
        /// При нажатии на кнопку, происходит передача команды PGDN (включить предыдущий слайд).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async private void ButtonNext_Clicked(object sender, EventArgs e)
        {
            url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/sendbuttons/";
            string textcomande = "{PGDN}";
            try
            {
                comande.Buttons.Add(textcomande);
                HttpResponseMessage response = await PowerPointClient.PostAsJsonAsync(url, comande);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
            comande.Buttons.Clear();
        }
        /// <summary>
        /// При нажатии на кнопку, происходит передача команды Alt+F4 (закрытие приложения).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async private void ButtonExit_Clicked(object sender, EventArgs e)
        {
            url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/sendbuttons/";
            string textcomande = "%{F4}";
            try
            {
                comande.Buttons.Add(textcomande);
                HttpResponseMessage response = await PowerPointClient.PostAsJsonAsync(url, comande);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
            comande.Buttons.Clear();
        }
    }
}
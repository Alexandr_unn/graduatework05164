﻿using SendMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SendMobileApp.View.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HandlePage : ContentPage
    {
        HandleViewModels viewModel;
        static readonly HttpClient HandelClient = new HttpClient();
        string url = "";
        List<Handleinfo> procinfo;
        public HandlePage()
        {
            procinfo = new List<Handleinfo>();
            InitializeComponent();
            BindingContext = viewModel = new HandleViewModels();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (viewModel.Items.Count == 0)
                viewModel.HandleItemsCommand.Execute(null);
        }
        /// <summary>
        /// Отправка команд на сервер 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void OnItemSelected(object sender, ItemTappedEventArgs args)
        {
            procinfo = viewModel.procinfo;
            url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/windows/";
            try
            {
                if (procinfo.Count != 0)
                {
                    int tee = args.ItemIndex;
                    url += procinfo[args.ItemIndex].Id;
                    HttpResponseMessage response = await HandelClient.GetAsync(url);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    url = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/windows/";
                }
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
        }
    }
}
﻿using SendMobileApp.Models.ComplJS;
using SendMobileApp.Models.TraficInfo;
using SendMobileApp.Services;
using SendMobileApp.View.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SendMobileApp.View.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NetworkInterfacePage : ContentPage
    {
        static readonly HttpClient NetIntlClient = new HttpClient();
        static List<NetworkInfo> netinfo;
        static PropertyInfo[] properties;
        public static int controlnumber;
        public NetworkInterfacePage()
        { 
            netinfo = new List<NetworkInfo>();
            InitializeComponent();
            StartFuncAsync();
            Type tp = typeof(NetworkInfo);
            properties = tp.GetProperties();
        }
        /// <summary>
        /// Получаю команды с сервера web api.
        /// </summary>
        async void StartFuncAsync()
        {
            string url1 = "http://" + MyAppConfig.ConfigurationManager.AppSettings.Get("Ip") + ":" + MyAppConfig.ConfigurationManager.AppSettings.Get("Port") + "/api/NetworkInterface";
            try
            {
                HttpResponseMessage response1 = await NetIntlClient.GetAsync(url1);
                response1.EnsureSuccessStatusCode();
                string responseBody = await response1.Content.ReadAsStringAsync();
                netinfo = NetworkInfo.FromJson(responseBody);
                for (int i = 0; i < netinfo.Count; i++)
                {
                    PickerApp.Items.Add(netinfo[i].Description);
                }
            }
            catch (HttpRequestException a)
            {
                await DisplayAlert("Exception ", a.Message, "Ok");
            }
        }
        /// <summary>
        /// При выборе сетевого адаптера выводит информацию на экран.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ItemPickerSelected(object sender, EventArgs e)
        {
            wcwcq.ItemsSource = GetSysInfo(PickerApp.SelectedIndex);
        }
        /// <summary>
        /// Формирую данные о сетевых адаптерах.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        static public List<ItemSysInfo> GetSysInfo(int i)
        {
            List<ItemSysInfo> retlis = new List<ItemSysInfo>();

            retlis.Add(new ItemSysInfo { Title = properties[1].Name, Data = netinfo[i].InterfaceType });
            retlis.Add(new ItemSysInfo { Title = properties[2].Name, Data = netinfo[i].PhysicalAddress });
            retlis.Add(new ItemSysInfo { Title = properties[3].Name, Data = netinfo[i].OperationalStatus});
            retlis.Add(new ItemSysInfo { Title = properties[4].Name, Data = netinfo[i].IpVersion});
            retlis.Add(new ItemSysInfo { Title = properties[5].Name, Data = netinfo[i].DnsSuffix });
            retlis.Add(new ItemSysInfo { Title = properties[6].Name, Data = netinfo[i].Mtu.ToString()});
            retlis.Add(new ItemSysInfo { Title = properties[7].Name, Data = netinfo[i].DnsEnabled  });
            retlis.Add(new ItemSysInfo { Title = properties[8].Name, Data = netinfo[i].DynamicallyConfiguredDns });
            retlis.Add(new ItemSysInfo { Title = properties[9].Name, Data =  netinfo[i].ReceiveOnly });
            retlis.Add(new ItemSysInfo { Title = properties[10].Name, Data = netinfo[i].Multicast });
            
            return retlis;
        }
        /// <summary>
        /// Выбор сетевого адаптера, и переход на соответствующую страницу сведений 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void GetTrafic(object sender, EventArgs e)
        {
            if (PickerApp.SelectedIndex >=0)
            {
                controlnumber = PickerApp.SelectedIndex;
                await Navigation.PushAsync(new TraficInfoPage()); 
            }
            else
            {
                await DisplayAlert("Ошибка", "Адаптер не выбран!", "Ok");
            }
        }
    }
}